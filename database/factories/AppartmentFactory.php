<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Appartment;
use Faker\Generator as Faker;

$factory->define(Appartment::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'slug' => $faker->slug,
    ];
});
