<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Appartment;
use App\Renting;
use Faker\Generator as Faker;

$factory->define(Renting::class, function (Faker $faker) {
    return [
        'appartment_id' => factory(Appartment::class)->create()->id,
    ];
});
