<?php

return [
    'intro_message' => 'Nous espérons que vous ayez passé un bon séjour.  Merci de nous laissez un commentaire afin que nous puissions nous améliorier la prochaine fois',
    'options' => [
        'selected' => 'Choisissez une note qui résume votre expérience',
        '0' => 'C\'était très mauvais!!',
        '1' => 'Je n\'ai pas vraiment apprécié',
        '2' => 'Je m\'attendais à mieux, je suis un peu déçu',
        '3' => 'Normal, pas grand chose à dire.',
        '4' => 'Bien! J\'ai vraiment passé du bon temps ici',
        '5' => 'Beaucoup mieux que ce j\'imaginais!',
    ],
    'comment' => [
        'label' => 'Dites nous ce que vous pensez',
    ],
    'email' => [
        'label' => 'Optionnellement, vous pouvez laissez votre adresse email afin que nous puissions vous recontacter',
        'help' => 'Nous ne partagerons jamais votre adresse avec personne.',
    ],
    'submit' => 'Soumettre',

    'thank_you' => 'Merci!',
];
