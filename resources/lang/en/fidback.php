<?php

return [
    'intro_message' => 'We hope you spent a good trip.  Please, help us so we can get even better next time!',
    'options' => [
        'selected' => 'Give a note to your overal experience',
        '0' => 'It was really bad !!',
        '1' => 'I did not enjoy it...',
        '2' => 'I expected more, just a little disapointed.',
        '3' => 'Yeah, normal, nothing much to say.',
        '4' => 'Good! I had a good time here.',
        '5' => 'Much better than I could have expected!',
    ],
    'comment' => [
        'label' => 'Please let us know what you think',
    ],
    'email' => [
        'label' => 'Optionnaly, you may leave your email so we can reach you about your experience.',
        'help' => 'We\'ll never share your email with anyone else.',
    ],
    'submit' => 'Submit',

    'thank_you' => 'Thank you !',
];
