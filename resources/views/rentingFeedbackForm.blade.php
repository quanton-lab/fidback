<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>{{ $appartment->name }}</title>
        <link rel="stylesheet" href="/css/app.css">
        <style>
            html,
            body {
                height: 100%;
            }
            .cover-container {
                max-width: 42em;
            }
        </style>
    </head>
    <body class="text-center ">
        <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
            <header class="masthead mb-auto"></header>
            <main role="main" class="inner cover">
                <h1>{{ $appartment->name }}</h1>
                <p>
                {{
                    __('fidback.intro_message')
                }}
                </p>
                <form action="{{ \LaravelLocalization::localizeURL(route('web.appartment.postFeedback', $appartment->slug)) }}" method="post">
                    @csrf
                    <div class="form-group">
                        <select class="custom-select" id="note" name="note">
                            <option selected>{{ __('fidback.options.selected') }}</option>
                            <option value="5">5 - {{ __('fidback.options.5') }}</option>
                            <option value="4">4 - {{ __('fidback.options.4') }}</option>
                            <option value="3">3 - {{ __('fidback.options.3') }}</option>
                            <option value="2">2 - {{ __('fidback.options.2') }}</option>
                            <option value="1">1 - {{ __('fidback.options.1') }}</option>
                            <option value="0">0 - {{ __('fidback.options.0') }}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="comment">{{  __('fidback.comment.label') }}</label>
                        <textarea class="form-control" id="comment" name="comment" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">{{ __('fidback.email.label') }}</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp">
                        <small id="emailHelp" class="form-text text-muted">{{ __('fidback.email.help') }}</small>
                    </div>
                    <button type="submit" class="btn btn-primary">{{ __('fidback.submit') }}</button>
                </form>
            </main>
            <footer class="mastfoot mt-auto">Proudly made by Quanton SPRL</footer>
        </div>

        <script src="/js/app.js"></script>
    </body>
</html>