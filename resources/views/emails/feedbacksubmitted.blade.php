@component('mail::message')
# Feedback submitted

A new feedback has been submitted on {{ config('app.name') }}. 

It concerns **{{ $appartmentName }}**

@component('mail::panel')
## Note: {{ $note }}


### Comment: 
{{ $comment }}


### From:
{{ $renter->email ??  '' }}

@endcomponent

{{-- @component('mail::button', ['url' => $url])
View Feedback
@endcomponent --}}

{{-- Thanks,<br>
{{ config('app.name') }} --}}
@endcomponent