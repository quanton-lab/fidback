<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Thank you</title>
        <link rel="stylesheet" href="/css/app.css">
        <style>
            html,
            body {
                height: 100%;
            }
            .cover-container {
                max-width: 42em;
            }
        </style>
    </head>
    <body class="text-center ">
        <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
            <header class="masthead mb-auto"></header>
            <main role="main" class="inner cover">
                <h1>{{ __('fidback.thank_you') }}</h1>
            </main>
            <footer class="mastfoot mt-auto">Proudly made by Quanton SPRL</footer>
        </div>

        <script src="/js/app.js"></script>
    </body>
</html>