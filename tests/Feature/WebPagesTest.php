<?php

namespace Tests\Feature;

use App\Appartment;
use App\Mail\FeedbackSubmitted;
use App\Renting;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use QuantonLab\Feedback\Model\Feedback;
use Tests\TestCase;

class WebPagesTest extends TestCase
{
    use RefreshDatabase;

    /***
     * @var Appartment
     */
    protected $appartment;

    /***
     * @var string
     */
    protected $appartmentUrl;

    public function setUp(): void
    {
        parent::setUp();

        Mail::fake();

        $this->appartment = factory(Appartment::class)->create();
        $this->appartmentUrl = '/' . $this->appartment->slug;
    }

    public function testGetAppartmentPageReturnsFormPage()
    {
        $response = $this->get($this->appartmentUrl);

        $response->assertOk();
    }

    public function testGetAppartmentPageDontWorkWhenNoAppartment()
    {
        $response = $this->get('/nochancethatthiswillwork');

        $response->assertNotFound();
    }

    public function testThankYouPageIsWorking()
    {
        $response = $this->get('/thankyou');

        $response->assertOk();
    }

    public function testPostFeedbackWillAssociatedWithRenting()
    {
        $response = $this->post($this->appartmentUrl, [
            'note' => 4,
            'comment' => 'this is my comment',
            'email' => 'testPostFeedbackWillCreateAFeedbackInDB@webpagetest.php',
        ]);

        $this->assertTrue($this->appartment->rentings()->firstOrNew([])->getFeedbacks()->count() > 0);
    }

    public function testPostFeedbackWillRedirectToThankYouPage()
    {
        factory(Renting::class)->create([
            'appartment_id' => $this->appartment->id,
        ]);

        $response = $this->post($this->appartmentUrl, [
            'note' => 4,
            'comment' => 'this is my comment',
            'email' => 'testPostFeedbackWillCreateAFeedbackInDB@webpagetest.php',
        ]);

        $response->assertRedirect('/thankyou');
    }

    public function testPostFeedbackWillAssociatedWithRenter()
    {
        $response = $this->post($this->appartmentUrl, [
            'note' => 4,
            'comment' => 'this is my comment',
            'email' => 'testPostFeedbackWillCreateAFeedbackInDB@webpagetest.php',
        ]);

        /***
         * @var Feedback
         */
        $feedback = $this->appartment->rentings()->firstOrNew([])->getFeedbacks()->last();
        $renter = $feedback->getFeedbacker();
        $this->assertEquals('testPostFeedbackWillCreateAFeedbackInDB@webpagetest.php', $renter->email);
    }

    public function testEmailIsOptionalOnPostFeedback()
    {
        $response = $this->post($this->appartmentUrl, [
            'note' => 4,
            'comment' => 'this is my comment',
        ]);

        $response->assertRedirect('/thankyou');
    }

    public function testCommentIsOptionalOnPostFeedback()
    {
        $response = $this->post($this->appartmentUrl, [
            'note' => 4,
        ]);

        $response->assertRedirect('/thankyou');
    }

    public function testEmailIsSentCorrectly()
    {
        $response = $this->post($this->appartmentUrl, [
            'note' => 4,
            'comment' => 'this is my comment',
            'email' => 'testEmailIsSentCorrectly@webpagetest.php',
        ]);

        Mail::assertSent(FeedbackSubmitted::class);
    }

    public function testEmailIsSentCorrectlyWhenNoEmail()
    {
        $response = $this->post($this->appartmentUrl, [
            'note' => 4,
            'comment' => 'this is my comment',
            // 'email' => 'testEmailIsSentCorrectly@webpagetest.php',
        ]);

        Mail::assertSent(FeedbackSubmitted::class);
    }

    public function testEmailIsSentCorrectlyWhenNoComment()
    {
        $response = $this->post($this->appartmentUrl, [
            'note' => 4,
            // 'comment' => 'this is my comment',
            'email' => 'testEmailIsSentCorrectly@webpagetest.php',
        ]);

        Mail::assertSent(FeedbackSubmitted::class);
    }
}
