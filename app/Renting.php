<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use QuantonLab\Feedback\Model\Feedbackable;
use QuantonLab\Feedback\Traits\IsFeedbackable;

class Renting extends Model implements Feedbackable
{
    use IsFeedbackable;

    const TABLE_NAME = 'rentings';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = self::TABLE_NAME;

    public function appartment(): BelongsTo
    {
        return $this->belongsTo(Appartment::class);
    }

    public function getAppartment(): Appartment
    {
        return $this->appartment;
    }
}
