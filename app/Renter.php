<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use QuantonLab\Feedback\Model\Feedbacker;

class Renter extends Model implements Feedbacker
{
    const TABLE_NAME = 'renters';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = self::TABLE_NAME;

    public function getEmail(): string
    {
        return $this->email;
    }
}
