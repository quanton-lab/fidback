<?php

namespace App\Formatter;

use App\Renting;
use Illuminate\Database\Eloquent\Collection;

class RentingFormatter
{
    public function formatItem(Renting $renting): array
    {
        return [
            'id' => $renting->id,
            'average_note' => $renting->getAverageNote(),
            'links' =>
            [
                [
                    'rel' => 'feedbacks',
                    'href' => route('renting.getfeedback', $renting),
                    'action' => 'GET'
                ],
                [
                    'rel' => 'feedbacks',
                    'href' => route('renting.postfeedback', $renting),
                    'action' => 'POST'
                ],
                [
                    'rel' => 'appartments',
                    'href' => route('appartments.show', $renting->appartment()->first()->name),
                    'action' => 'GET'
                ],
                [
                    'rel' => 'self',
                    'href' => route('rentings.show', $renting),
                    'action' => 'GET'
                ],
                [
                    'rel' => 'self',
                    'href' => route('rentings.update', $renting),
                    'action' => 'PUT'
                ],
                [
                    'rel' => 'self',
                    'href' => route('rentings.destroy', $renting),
                    'action' => 'DELETE'
                ],
            ]
        ];
    }

    public function formatList(Collection $rentings): array
    {
        return $rentings->map(function ($item) {
            return $this->formatItem($item);
        })->toArray();
    }
}
