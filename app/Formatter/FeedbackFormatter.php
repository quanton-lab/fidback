<?php

namespace App\Formatter;

use Illuminate\Database\Eloquent\Collection;
use QuantonLab\Feedback\Model\Feedback;

class FeedbackFormatter
{
    public function formatItem(Feedback $feedback): array
    {
        return [
            'note' => $feedback->getNote(),
            'comment' => $feedback->getComment(),
        ];
    }

    public function formatList(Collection $feedbacks): array
    {
        return $feedbacks->map(function ($item) {
            return $this->formatItem($item);
        })->toArray();
    }
}
