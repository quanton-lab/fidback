<?php

namespace App\Services;

use App\Appartment;
use App\Renter;
use App\Renting;
use QuantonLab\Feedback\Exceptions\TooHighNoteException;
use QuantonLab\Feedback\Exceptions\TooLowNoteException;
use QuantonLab\Feedback\Model\Feedback;
use QuantonLab\Feedback\Services\FeedbackService;

class PostFeedbackService
{
    protected $feedbackService;

    public function __construct(FeedbackService $feedbackService)
    {
        $this->feedbackService = $feedbackService;
    }

    public function createFeedbackServiceOnAppartment(Appartment $appartment, int $note, ?string $comment, ?string $email): Feedback
    {
        $renting = $appartment->getCurrentRentingOrCreate();

        return $this->createFeedbackServiceOnRenting($renting, $note, $comment, $email);
    }

    public function createFeedbackServiceOnRenting(Renting $renting, int $note, ?string $comment, ?string $email): Feedback
    {
        $renter = null;
        if (isset($email)) {
            $renter = $this->createRenterFromEmail($email);
        }

        try {
            return $this->feedbackService->create($renting, $note, $comment, $renter);
        } catch (TooLowNoteException $e) {
            abort(422, 'The Note may not be negative');
        } catch (TooHighNoteException $e) {
            abort(422, 'The note may not be higher than 5');
        }
    }

    protected function createRenterFromEmail(string $email): Renter
    {
        $renter = Renter::make();
        $renter->email = $email;
        $renter->save();

        return $renter;
    }
}
