<?php

namespace App\Http\Controllers\Web;

use App\Appartment;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostFeedbackRequest;
use App\Mail\FeedbackSubmitted;
use App\Services\PostFeedbackService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class WebController extends Controller
{
    /***
     * @var PostFeedbackService
     */
    protected $postFeedbackService;

    public function __construct(PostFeedbackService $postFeedbackService)
    {
        $this->postFeedbackService = $postFeedbackService;
    }

    public function appartmentPage(Request $request, Appartment $appartment)
    {
        return view('rentingFeedbackForm', [
            'appartment' => $appartment,
        ]);
    }

    public function postFeedback(PostFeedbackRequest $request, Appartment $appartment)
    {
        $feedback = $this->postFeedbackService->createFeedbackServiceOnAppartment(
            $appartment,
            $request->input('note'),
            $request->input('comment'),
            $request->input('email')
        );

        Mail::send(new FeedbackSubmitted($feedback));

        return redirect()->route('web.thankyou');
    }

    public function thankyou()
    {
        return view('thankyou');
    }
}
