<?php

namespace App\Http\Controllers\Api;

use App\Appartment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AppartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Appartment::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $appartment = Appartment::make();
        $appartment->name = $request->input('name');
        $appartment->slug = $request->input('slug');
        $appartment->save();

        return response($appartment, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appartment  $appartment
     * @return \Illuminate\Http\Response
     */
    public function show(Appartment $appartment)
    {
        return response()->json($appartment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appartment  $appartment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appartment $appartment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appartment  $appartment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appartment $appartment)
    {
        //
    }
}
