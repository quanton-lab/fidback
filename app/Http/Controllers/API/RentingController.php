<?php

namespace App\Http\Controllers\API;

use App\Appartment;
use App\Formatter\FeedbackFormatter;
use App\Formatter\RentingFormatter;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostFeedbackRequest;
use App\Renting;
use App\Services\PostFeedbackService;
use Illuminate\Http\Request;

class RentingController extends Controller
{
    /***
     * @var PostFeedbackService
     */
    protected $postFeedbackService;

    /***
     * @var RentingFormatter
     */
    protected $rentingFormatter;

    /***
     * @var FeedbackFormatter
     */
    protected $feedbackFormatter;

    public function __construct(
        PostFeedbackService $postFeedbackService,
        RentingFormatter $rentingFormatter,
        FeedbackFormatter $feedbackFormatter
    ) {
        $this->postFeedbackService = $postFeedbackService;
        $this->rentingFormatter = $rentingFormatter;
        $this->feedbackFormatter = $feedbackFormatter;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->rentingFormatter->formatList(Renting::all()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $renting = Renting::make();
        $renting->appartment()->associate(Appartment::firstOrCreate([
            'name' => $request->input('appartment')
        ]));
        $renting->save();

        return response($this->rentingFormatter->formatItem($renting), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Renting  $renting
     * @return \Illuminate\Http\Response
     */
    public function show(Renting $renting)
    {
        return response()->json($this->rentingFormatter->formatItem($renting));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Renting  $renting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Renting $renting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Renting  $renting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Renting $renting)
    {
        //
    }

    public function getFeedbacks(Renting $renting)
    {
        $feedbacks = $renting->getFeedbacks();

        return response()->json($this->feedbackFormatter->formatList($feedbacks));
    }

    public function postFeedbacks(PostFeedbackRequest $request, Renting $renting)
    {
        $feedback = $this->postFeedbackService->createFeedbackServiceOnRenting(
            $renting,
            $request->input('note'),
            $request->input('comment'),
            $request->input('email')
        );

        return response()->json($this->feedbackFormatter->formatItem($feedback), 201);
    }
}
