<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Appartment extends Model
{
    const TABLE_NAME = 'appartments';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = self::TABLE_NAME;

    public function rentings(): HasMany
    {
        return $this->hasMany(Renting::class);
    }

    public function getCurrentRentingOrCreate(): Renting
    {
        return $this->rentings()->firstOrCreate([]);
    }

    /**
     * Retrieve the model for a bound value.
     *
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function resolveRouteBinding($value)
    {
        return $this->where('slug', $value)->firstOrFail();
    }
}
