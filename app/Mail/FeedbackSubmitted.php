<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use QuantonLab\Feedback\Model\Feedback;

class FeedbackSubmitted extends Mailable
{
    use Queueable, SerializesModels;

    /***
     * @var Feedback
     */
    protected $feedback;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Feedback $feedback)
    {
        $this->feedback = $feedback;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        /***
         * @var \App\Renting
         */
        $renting = $this->feedback->getFeedbackable();

        /***
         * @var \App\Renter
         */
        $renter = $this->feedback->getFeedbacker();

        return $this->to(config('fidback.adminEmail'))
            ->subject('Feedback submitted')
            ->markdown('emails.feedbacksubmitted')
            ->with([
                'appartmentName' => $renting->getAppartment()->name,
                'note' => $this->feedback->getNote(),
                'comment' => $this->feedback->getComment(),
                'renter' => isset($renter) ? $renter->getEmail() : null,
            ]);
    }
}
