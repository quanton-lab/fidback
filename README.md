# Fidback application

### Connect to API

API uses the OAuth's client-credential protocol to secure API routes.  
1. Retrieve the token with your client id and your client key
2. Call the api routs with the "Authorization" header with value `Bearer token`